package validate

import "errors"

var (
	// ErrRequired occurs when a required field has it's default value
	ErrRequired = errors.New("is required")

	// ErrNotNilable occurs when a field that is not nilable is nil
	ErrNotNilable = errors.New("can't be nil")
)
