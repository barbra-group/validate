package validate

import (
	"fmt"
	"time"
)

// IsOlderThan returns a TimeRule to verify that a person with the given birthday is older than the given age.
func IsOlderThan(age int) TimeRule {
	return NewTimeRule(func(t time.Time) bool {
		now := time.Now()
		y := now.Year() - t.Year()
		if now.YearDay() < t.YearDay() {
			y--
		}
		return age >= y
	}, fmt.Sprintf("should be at least %d years old", age))
}
