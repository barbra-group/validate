<p align="center" style="text-align:center">
    <img src="https://gitlab.com/barbra-group/validate/raw/master/logo.svg" />
</p>

# Welcome to Blaze

<p align="center">
  <b>Blaze</b> is a <b>balzing fast</b> and <b>extensible</b> validation librarywritten in the <b>GO</b> programming language. It <b>doesn't rely on relfection</b> and <b>isn't based on error-prone struct tags</b>. Blaze does support <b>pointer, slices and maps</b> by default.
</p>

## Rules
### String
Name					| Description
------------------------|----------------
IsURL					| Asserts a string to be a valid URL
IsEmail					| Asserts a string to be e valid Email Address
IsUpperCase				| Asserts a string to be uppercase
IsLowerCase				| Asserts a string to be lowercase
IsAlphaUnicodeNumeric	| Asserts a string to only contain alphanumeric unicode chars
IsAlphaUnicode			| Asserts a string to only contain alphabetic unicode chars
IsAlphaNumeric			| Asserts a string to only contain alphanumeric ASCII chars
IsNumeric				| Asserts a string to only contain numeric chars
IsAlpha					| Asserts a string to only contain alphabetic chars
IsUUIDv5				| Asserts a string to be a valid UUID Version 5
IsUUIDv4				| Asserts a string to be a valid UUID Version 4
IsUUIDv3				| Asserts a string to be a valid UUID Version 3
IsUUID					| Asserts a string to be a valid UUID
HasRuneLength(min, max) | Asserts the string to contain between min and max chars

### String Pointer
String pointer can use all string validation rules.

### String Slices
Name						| Description
----------------------------|----------------
AllStringRules(rules)		| Asserts that all strings in the slice comply with all given string rules
HasStringSliceSize(min, max)| Asserts the slices to contain between min and max items

### Time

Name						| Description
----------------------------|----------------
IsOlderThan(age)			| Asserts that a person with the given time as birthday would be older than the given age

### Example
```go
// Params are the parameters allowed during creation/updates of the collections.
type Params struct {
	Description *string
	Title       *string
	Tags        []string
}


// Validate validates the node params. If all fields are valid the error will be nil.
func (p *Params) Validate() error {
	return validate.Fields("collection params",
		validate.StringPtr(p.Description, "description", true, true, validate.HasRuneLength(1, 5000)),
		validate.StringPtr(p.Title, "title", true, false, validate.HasRuneLength(1, 200)),
		validate.StringSlice(p.Tags, "tags", true, validate.AllStringRules(validate.HasRuneLength(2, 50)), validate.HasStringSliceSize(0, 50)),
	)
}
```