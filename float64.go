package validate

import (
	"fmt"
)

// IsInRageFloat64 returns a Float64Rule to verify that the value of a float64 in the given range (including the values of min and max).
func IsInRageFloat64(min, max float64) Float64Rule {
	return NewFloat64Rule(func(f float64) bool {
		return f >= min && f <= max
	}, fmt.Sprintf("should be >= %f and <= %f", min, max))
}

// IsBetweenFloat64 returns a Float64Rule to verify that the value of a float64 is between the given values (not including the values of min and max).
func IsBetweenFloat64(min, max float64) Float64Rule {
	return NewFloat64Rule(func(f float64) bool {
		return f > min && f < max
	}, fmt.Sprintf("should be > %f and < %f", min, max))
}
