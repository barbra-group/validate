package validate

import "errors"

// StringSliceValidator is a function used to validate string slices.
type StringSliceValidator func(s []string) bool

// StringSlice validates a string slice using the given rules.
func StringSlice(val []string, fieldName string, optional bool, rules ...StringSliceRule) error {
	if !optional && val == nil {
		return &Error{fieldName, ErrRequired.Error()}
	}

	if val == nil {
		return nil
	}

	for _, rule := range rules {
		if err := rule.Validate(val); err != nil {
			return &Error{fieldName, err.Error()}
		}
	}

	return nil
}

type StringSliceRule interface {
	Validate(s []string) error
}

// NewStringSliceRule creates a new string slice rule using the given validator and error message.
func NewStringSliceRule(validator StringSliceValidator, msg string) StringSliceRule {
	return &stringSliceRule{validator, msg}
}

type stringSliceRule struct {
	validator StringSliceValidator
	msg       string
}

// Validates checks whether the sting is compliant to the string slice rule. If not an error will be returned.
func (r stringSliceRule) Validate(s []string) error {
	if !r.validator(s) {
		return errors.New(r.msg)
	}

	return nil
}
