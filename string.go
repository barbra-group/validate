package validate

import (
	"fmt"
	"strings"
)

var (
	IsURL                 = NewStringRule(urlRegex.MatchString, "should be a valid url")
	IsEmail               = NewStringRule(emailRegex.MatchString, "should be a valid email")
	IsUpperCase           = NewStringRule(isUpperCase, "should be uppercase")
	IsLowerCase           = NewStringRule(isLowerCase, "should be lowercase")
	IsAlphaUnicodeNumeric = NewStringRule(alphaUnicodeNumericRegex.MatchString, "should only contain alphanumeric unicode chars")
	IsAlphaUnicode        = NewStringRule(alphaUnicodeRegex.MatchString, "should only contain alphabetic unicode chars")
	IsAlphaNumeric        = NewStringRule(alphaNumericRegex.MatchString, "should only contain alphanumeric chars")
	IsNumeric             = NewStringRule(numericRegex.MatchString, "should only contain numeric chars")
	IsAlpha               = NewStringRule(alphaRegex.MatchString, "should only contain alphabetic chars")
	IsUUIDv5              = NewStringRule(uUID5Regex.MatchString, "should be a valid UUIDv5")
	IsUUIDv4              = NewStringRule(uUID4Regex.MatchString, "should be a valid UUIDv4")
	IsUUIDv3              = NewStringRule(uUID3Regex.MatchString, "should be a valid UUIDv3")
	IsUUID                = NewStringRule(uUIDRegex.MatchString, "should be a valid UUID")
)

func isUpperCase(s string) bool {
	return s == strings.ToUpper(s)
}

func isLowerCase(s string) bool {
	return s == strings.ToLower(s)
}

// HasRuneLength returns a StringRule to verify that the length of a string is between the given rune length range.
func HasRuneLength(min, max int) StringRule {
	return NewStringRule(func(s string) bool {
		return len(s) >= min && len(s) <= max
	}, fmt.Sprintf("should be between %d and %d chars long", min, max))
}

// DisallowStringValue returns a StringRule to verify that a string does not match the given input.
// If ignoreCase is set to true, the casing will be ignored
func DisallowStringValue(value string, ignoreCase bool) StringRule {
	if ignoreCase {
		value = strings.ToLower(value)
	}

	return NewStringRule(func(s string) bool {
		if !ignoreCase {
			return s != value
		}

		return strings.ToLower(s) != value
	}, fmt.Sprintf("should not be %s", value))
}
