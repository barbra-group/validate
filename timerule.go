package validate

import (
	"errors"
	"time"
)

// TimeValidator is a function used to validate time.Time objects.
type TimeValidator func(t time.Time) bool

// TimePtr validates a time.Time pointer using the given rules.
func TimePtr(val *time.Time, fieldName string, nilable, allowDefault bool, rules ...TimeRule) error {
	if !nilable && val == nil {
		return &Error{fieldName, ErrNotNilable.Error()}
	}

	if val == nil {
		return nil
	}

	return Time(*val, fieldName, allowDefault, rules...)
}

// Time validates a time.Time object using the given rules.
func Time(val time.Time, fieldName string, optional bool, rules ...TimeRule) error {
	if !optional && val.IsZero() {
		return &Error{fieldName, ErrRequired.Error()}
	}

	if val.IsZero() {
		return nil
	}

	for _, rule := range rules {
		if err := rule.Validate(val); err != nil {
			return &Error{fieldName, err.Error()}
		}
	}

	return nil
}

type TimeRule interface {
	Validate(t time.Time) error
}

// NewTimeRule creates a new time rule using the given validator and error message.
func NewTimeRule(validator TimeValidator, msg string) TimeRule {
	return &timeRule{validator, msg}
}

type timeRule struct {
	validator TimeValidator
	msg       string
}

// Validates checks whether the time.Time object is compliant to the time rule. If not an error will be returned.
func (r timeRule) Validate(t time.Time) error {
	if !r.validator(t) {
		return errors.New(r.msg)
	}

	return nil
}
