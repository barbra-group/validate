package validate

import (
	"errors"
)

// Float64Validator is a function used to validate float64s.
type Float64Validator func(f float64) bool

// Float64Ptr validates a float64 pointer using the given rules.
func Float64Ptr(val *float64, fieldName string, nilable bool, rules ...Float64Rule) error {
	if !nilable && val == nil {
		return &Error{fieldName, ErrNotNilable.Error()}
	}

	return Float64(*val, fieldName, rules...)
}

// Float64 validates a float64 using the given rules.
func Float64(val float64, fieldName string, rules ...Float64Rule) error {
	for _, rule := range rules {
		if err := rule.Validate(val); err != nil {
			return &Error{fieldName, err.Error()}
		}
	}

	return nil
}

type Float64Rule interface {
	Validate(f float64) error
}

// NewFloat64Rule creates a new float64 rule using the given validator and error message.
func NewFloat64Rule(validator Float64Validator, msg string) Float64Rule {
	return &float64Rule{validator, msg}
}

type float64Rule struct {
	validator Float64Validator
	msg       string
}

// Validates checks whether the float64 object is compliant to the float64 rule. If not an error will be returned.
func (r float64Rule) Validate(f float64) error {
	if !r.validator(f) {
		return errors.New(r.msg)
	}

	return nil
}
