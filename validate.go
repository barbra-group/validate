package validate

import "reflect"

type validateable interface {
	Validate() error
}

// Validateable validates a object that implements validateable
func Validateable(v validateable, name string, nilable bool) error {
	if !nilable && v == nil {
		return ErrNotNilable
	}

	if !nilable && reflect.ValueOf(v).IsNil() {
		return ErrNotNilable
	}

	if reflect.ValueOf(v).IsNil() {
		return nil
	}

	err := v.Validate()
	if e, ok := err.(FieldErrors); ok {
		e.name = name
		return err
	}

	return err
}

// Fields verifies multiple fields and returns FieldErrors
func Fields(name string, errors ...error) error {
	var errs []error
	for _, err := range errors {
		if err != nil {
			errs = append(errs, err)
		}
	}

	if errs != nil {
		return &FieldErrors{name, errs}
	}

	return nil
}
